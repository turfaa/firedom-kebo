import { create } from 'apisauce';

import { CONTEST_BASE_URL } from 'Config/contest';

const api = create({
  baseURL: `${CONTEST_BASE_URL}/api`,
});

export function getContestData(contestId) {
  return api.get('/contests').then((response) => {
    if (response.ok) {
      response.data = response.data[contestId];
    }

    return response;
  });
}

export function getScoreboard(contestId) {
  return api.get('/scoreboard', { cid: contestId });
}

export function getTeamsData() {
  return api.get('/teams');
}
