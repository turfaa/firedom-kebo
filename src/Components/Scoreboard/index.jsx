import React, { Component } from 'react';
import PropTypes from 'prop-types';

export const Score = ({ numSolved, totalTime }) => (
  <div>
    <div>{numSolved}</div>
    <div>({totalTime})</div>
  </div>
);

export const ProblemResult = ({
  numJudged, solved, time, firstToSolve,
}) => {
  let color = 'transparent';
  let isi;

  if (solved) {
    isi = (
      <div>
        <div>{time}</div>
        {numJudged > 1 && (<div>({numJudged - 1})</div>)}
      </div>
    );

    if (firstToSolve) color = '#4CAF50';
    else color = '#8BC34A';
  } else {
    isi = (
      <div>
        <div>-</div>
        {numJudged > 0 && (<div>({numJudged})</div>)}
      </div>
    );

    if (numJudged > 0) color = '#D50000';
  }

  return (
    <td style={{
 backgroundColor: color, textAlign: 'center', paddingLeft: 20, paddingRight: 20,
}}
    >{isi}
    </td>);
};

export default class Scoreboard extends Component {
  render() {
    const { scoreboard } = this.props;

    if (!scoreboard.length) {
      return null;
    }

    return (
      <div style={{ overflowX: 'auto' }}>
        <table>
          <thead>
            <tr>
              <th style={{ textAlign: 'center' }}>Rank</th>
              <th>Name</th>
              <th>University</th>
              <th style={{ textAlign: 'center' }}>Score</th>

              {scoreboard[0].problems.map(problem => (
                <th style={{ textAlign: 'center', paddingLeft: 20, paddingRight: 20 }} key={`problem${problem.label}`}>{problem.label}</th>
              ))}
            </tr>
          </thead>

          <tbody>
            {scoreboard.map(obj => (
              <tr key={`team${obj.team.id}`}>
                <td style={{ textAlign: 'center' }}>{obj.rank}</td>
                <td>{obj.team.name}</td>
                <td>{obj.team.affiliation}</td>
                <td style={{ textAlign: 'center' }}><Score numSolved={obj.score.num_solved} totalTime={obj.score.total_time} /></td>

                {obj.problems.map(problem => (
                  <ProblemResult
                    numJudged={problem.num_judged}
                    solved={problem.solved}
                    numPending={problem.num_pending}
                    time={problem.time}
                    firstToSolve={problem.first_to_solve}
                    key={`team${obj.team.id}problem${problem.label}`}
                  />
                ))}
              </tr>
            ))}
          </tbody>
        </table>
      </div>
    );
  }
}

Score.propTypes = {
  numSolved: PropTypes.number.isRequired,
  totalTime: PropTypes.number.isRequired,
};

ProblemResult.propTypes = ({
  numJudged: PropTypes.number.isRequired,
  solved: PropTypes.bool.isRequired,
  time: PropTypes.number.isRequired,
  firstToSolve: PropTypes.bool.isRequired,
});

Scoreboard.propTypes = {
  scoreboard: PropTypes.arrayOf(PropTypes.shape({
    rank: PropTypes.number.isRequired,
    team: PropTypes.shape({
      id: PropTypes.number.isRequired,
      name: PropTypes.string.isRequired,
      affiliation: PropTypes.string.isRequired,
    }).isRequired,
    score: PropTypes.shape({
      num_solved: PropTypes.number.isRequired,
      total_time: PropTypes.number.isRequired,
    }).isRequired,
    problems: PropTypes.arrayOf(PropTypes.shape({
      label: PropTypes.string.isRequired,
      num_judged: PropTypes.number.isRequired,
      num_pending: PropTypes.number.isRequired,
      solved: PropTypes.bool.isRequired,
      time: PropTypes.number.isRequired,
      first_to_solve: PropTypes.bool.isRequired,
    })).isRequired,
  })).isRequired,
};
