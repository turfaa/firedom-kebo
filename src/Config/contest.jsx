export const CONTEST_BASE_URL = 'https://arkavidia-proxy.herokuapp.com/programming.arkavidia.id/domjudge';
export const FETCH_INTERVAL = 60000;
export const CONTEST_ID = 5;
