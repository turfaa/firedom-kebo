import firebase from 'firebase';

const config = {
  apiKey: 'AIzaSyAbAgXS28evkQNSC-pbUmTxjJ6wl_kdHl4',
  authDomain: 'arkavidia-2018.firebaseapp.com',
  databaseURL: 'https://arkavidia-2018.firebaseio.com',
  projectId: 'arkavidia-2018',
  storageBucket: 'arkavidia-2018.appspot.com',
  messagingSenderId: '171815665337',
};

const fire = firebase.initializeApp(config);

export default fire;
