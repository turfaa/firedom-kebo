import React, { Component } from 'react';

import SiteHeader from 'Containers/SiteHeader';
import SiteContent from 'Containers/SiteContent';

import styles from 'styles';

class App extends Component {
  render() {
    return (
      <div style={styles.container}>
        <SiteHeader />
        <SiteContent />
      </div>
    );
  }
}

export default App;
