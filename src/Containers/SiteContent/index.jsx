import React, { Component } from 'react';

import { CONTEST_ID, FETCH_INTERVAL } from 'Config/contest';
import Scoreboard from 'Components/Scoreboard';

import { getTeamsData, getScoreboard } from 'Api/contest';

const initialState = {
  isScoreboardLoading: true,
  isTeamsLoading: true,

  scoreboard: [],
  teams: {},
};

export default class SiteContent extends Component {
  constructor(props) {
    super(props);

    this.state = initialState;
  }

  componentDidMount() {
    getTeamsData().then((response) => {
      if (response.ok) {
        this.updateTeams(response.data);
      }
    });

    getScoreboard(CONTEST_ID).then((response) => {
      if (response.ok) {
        this.updateScoreboard(response.data);
      }
    });

    setInterval((() => {
      getScoreboard(CONTEST_ID).then((response) => {
        if (response.ok) {
          this.updateScoreboard(response.data);
        }
      });
    }), FETCH_INTERVAL);
  }

  getTransformedScoreboard() {
    const { scoreboard, teams } = this.state;

    return scoreboard.map(obj => ({
      ...obj,
      team: teams[obj.team],
    }));
  }

  updateScoreboard(scoreboard) {
    if (scoreboard) {
      this.setState({ scoreboard });
    } else {
      this.setState({ scoreboard: [] });
    }

    this.setState({ isScoreboardLoading: false });
  }

  updateTeams(_teams) {
    if (_teams) {
      const teams = _teams.reduce((result, item) => {
        const transformedResult = { ...result };
        transformedResult[item.id] = item;
        return transformedResult;
      }, {});

      this.setState({ teams });
    }

    this.setState({ isTeamsLoading: false });
  }

  render() {
    const { isScoreboardLoading, isTeamsLoading } = this.state;

    if (isScoreboardLoading || isTeamsLoading) {
      return (
        <h2>Loading data...</h2>
      );
    }

    return (
      <div>
        <Scoreboard
          scoreboard={this.getTransformedScoreboard()}
        />
      </div>
    );
  }
}
