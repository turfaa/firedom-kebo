import React, { Component } from 'react';
import moment from 'moment';

import { getContestData } from 'Api/contest';
import { CONTEST_ID } from 'Config/contest';

const initialState = {
  contestData: {},
  counter: 0,
};

export default class SiteHeader extends Component {
  constructor(props) {
    super(props);

    this.state = initialState;
  }

  componentDidMount() {
    getContestData(CONTEST_ID).then((response) => {
      if (response.ok) {
        this.updateContestData(response.data);
      }
    });

    this.intervalId = setInterval(this.forceRerender.bind(this), 1000);
  }

  componentWillUnmount() {
    clearInterval(this.intervalId);
  }

  forceRerender() {
    this.setState(prevState => ({ counter: prevState.counter + 1 }));
  }

  updateContestData(contestData) {
    if (contestData) {
      this.setState({ contestData });
    }
  }

  render() {
    const { contestData } = this.state;
    const {
      freeze, unfreeze, start, end,
    } = contestData;

    let isFrozen = false;
    if (freeze) {
      if (moment.unix(freeze) <= moment()) {
        isFrozen = true;
      }
    }

    if (unfreeze) {
      if (moment.unix(unfreeze) <= moment()) {
        isFrozen = false;
      }
    }

    let startStatus; // 0 - not started, 1 - running, 2 - finished
    if (start) {
      if (moment() < moment.unix(start)) {
        startStatus = 0;
      } else {
        startStatus = 1;
      }
    }

    if (end) {
      if (moment.unix(end) <= moment()) {
        startStatus = 2;
      }
    }

    let timeStatus;
    if (typeof (startStatus) === 'undefined') {
      timeStatus = '';
    } else if (startStatus === 0) {
      timeStatus = `- Contest will start ${moment.unix(start).fromNow()}`;
    } else if (startStatus === 1) {
      timeStatus = `- Contest will end ${moment.unix(end).fromNow()}`;
    } else {
      timeStatus = `- Contest has ended ${moment.unix(end).fromNow()}`;
    }

    return (
      <div>
        {contestData && (
          <h1>{contestData.name}</h1>
        )}

        <h2>{isFrozen && 'Frozen '}Contest Scoreboard {timeStatus}</h2>
      </div>
    );
  }
}
