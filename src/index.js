import React from 'react';
import ReactDOM from 'react-dom';

import Typography from 'typography';
import theme from 'typography-theme-github';

import App from './App';
import registerServiceWorker from './registerServiceWorker';

const typography = new Typography(theme);

typography.injectStyles();

ReactDOM.render(React.createElement(App), document.getElementById('root'));
registerServiceWorker();
